#include <stdio.h>

int main(){
	
	printf("- - - - - Desigualdad de triangulos - - - - - \n\n");
	
	double lado1, lado2, lado3 =0;
	char mensaje_error[]="La suma de los primeros dos lados deben ser mayores que el ultimo lado ingresado";
	
	printf("Ingrese el valor del primer lado: ");
	scanf("%lf", &lado1);
	printf("Ingrese el valor del segundo lado: ");
	scanf("%lf", &lado2);
	printf("Ingrese el valor del tercer lado: ");
	scanf("%lf", &lado3);
	
	if(lado3<=lado1+lado2){
		printf("Es posible formar un triangulo\n");
	}else{
		printf("%s", mensaje_error);
		}	
	
	return 0;
}
