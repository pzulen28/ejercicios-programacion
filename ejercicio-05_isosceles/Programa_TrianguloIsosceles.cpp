#include <stdio.h>

int main()
{
	int lado =2;
	float altura, base, perimetro = 0;
	char mensaje_error[]= "El valor ingresado debe ser mayor a 0";
	
	
	printf("-------Triangulo Isosceles-------\n\n");
	printf ("Ingresa el valor base \n");
	scanf("%f", &base);
	if(base>0){
		printf("Ingrese el valor de la altura \n");
		scanf("%f", &altura);
		if(altura>0){
			perimetro = (base + (altura * lado));
			printf("El perimetro del triangulo isosceles es: %.2f", perimetro);			
		}else{
			printf("%s", mensaje_error);
		}
	}else{
		printf("%s", mensaje_error);
	}

	return 0;
}
