#include <stdio.h>

int main(){
	printf("- - - - - PERIMETRO DE UN TRIANGULO - - - - -\n\n");
	
	int opcion;
	
	double lado1, lado2, lado3 =0;	
	float valor, perimetro, altura, base = 0;
	char mensaje_error[]= "El valor ingresado debe ser mayor a 0";
	
	do{
		printf("\n\nElija una opcion: ");
	printf ("\n1.- Triangulo Equilatero \n2.- Trinagulo Isosceles \n3.- Triangulo Escaleno \n4.- Salir\n");
	scanf("%d", &opcion);
	
	switch(opcion){
		
		case 1:
		printf("\n- - - - -Triangulo Equilatero- - - - -\n\n");	
		
		printf ("Ingrese el valor de los lados que tendra el triangulo equilatero: \n");
			scanf("%f", &valor);
			
				if(valor > 0){
					perimetro = valor * 3;
					printf("El perimetro del triangulo equilatero es: %.2f", perimetro);
				}else{
					printf("%s", mensaje_error);
				}
			
			break;
			
		case 2:
			printf("\n- - - - - Triangulo Isosceles- - - - - \n\n");
			
			printf ("Ingresa el valor base \n");
			scanf("%f", &base);
			
			if(base>0){
				
				printf("Ingrese el valor de la altura \n");
					scanf("%f", &altura);
					
					if(altura>0){
						
						perimetro = (base + (altura * 2));
						printf("El perimetro del triangulo isosceles es: %.2f", perimetro);	
									
					}else{
						printf("%s", mensaje_error);
					}
					}else{
						printf("%s", mensaje_error);
					}
					
			break;
				
		case 3:
			printf("\n- - - - - Triangulo Escaleno- - - - - \n\n");
			
			printf("Ingresa el valor del primer lado: ");
			scanf("%lf", &lado1);
			
		    if ( lado1 > 0 ) {
		    	
		        printf("\nIngresa el valor del segundo lado: ");
		        scanf("%lf", &lado2);
		        
		        if ( lado2 > 0 ) {
		        	
		            printf("\nIngresa el valor del tercer lado: ");
		            scanf("%lf", &lado3);
		            
		            if ( lado3 > 0  ) {
		            	
		                perimetro =lado1 + lado2 + lado3;
		                printf("El Perimetro del triangulo escaleno es: %.1lf", perimetro);
		                
		            } else {
		                printf("%s", mensaje_error);
		            }
		        } else {
		            printf("%s", mensaje_error);
		        }
		    } else {
		        printf("%s", mensaje_error);
		    }
			
			break;
							
	}
		
	}while (opcion!=4);
	
	return 0;
}
