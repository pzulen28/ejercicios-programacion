#include <stdio.h>

int main(){
	
	int tabla, i;
	
	printf("- - - - -  TABLAS DE MULTIPLICAR - - - - - \n \n");
	printf("Introduce en numero de la tabla que deseas visualizar del 2 al 10: ");
	scanf("%i", &tabla);
	
	if(tabla >= 2 && tabla <= 10){
		
		for(i = 1; i <= 10; i++){
			printf("\n %i  *  %i  = %i", tabla, i, tabla*i);
		}
	}else{
		printf("El rango que puedes elegir de la tabla es entre 2 al 10");
	}
	
	return 0;
}




