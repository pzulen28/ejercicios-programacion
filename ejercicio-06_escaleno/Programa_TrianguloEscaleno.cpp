#include <stdio.h>

int main()
{
    double lado1, lado2, lado3, perimetro=0;
    char mensaje_error[] = "el valor ingresado debe ser mayor a 0";
    
    printf("Ingresa el valor del primer lado: ");
    scanf("%lf", &lado1);
    if ( lado1 > 0 ) {
        printf("\nIngresa el valor del segundo lado: ");
        scanf("%lf", &lado2);
        if ( lado2 > 0 ) {
            printf("\nIngresa el valor del tercer lado: ");
            scanf("%lf", &lado3);
            if ( lado3 > 0  ) {
                perimetro =lado1 + lado2 + lado3;
                printf("El Perimetro es: %.1lf", perimetro);
            } else {
                printf("%s", mensaje_error);
            }
        } else {
            printf("%s", mensaje_error);
        }
    } else {
        printf("%s", mensaje_error);
    }
    return 0;
}
