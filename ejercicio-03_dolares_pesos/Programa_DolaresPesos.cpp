#include <stdio.h>

int main ()
{
	float dolar=22.39;
	float pesos=0;
	int montoDolares =0;
	char mensaje_error[]= "El monto ingresado debe ser mayor a 0";
	
	printf ("El precio actual del dolar es de $22.39\n\n");
	printf("Ingrese el monto  de dolares que desea cambiar a pesos: \n");
	scanf("%i", &montoDolares);
	if(montoDolares>0){
		 pesos = montoDolares * dolar;
		 printf("Su dinero en pesos es:$%.2f", pesos);
	}else{
		printf("%s", mensaje_error);
	}
	
	return 0;	
	
}
