#include <stdio.h>

int main()
{
	int lado =3;
	float valor, perimetro = 0;
	char mensaje_error[]= "El valor ingresado debe ser mayor a 0";
	
	printf ("Ingrese el valor de los lados que tendra el triangulo equilatero: \n");
	scanf("%f", &valor);
	if(valor>0){
		perimetro = valor * lado;
		printf("El perimero del triangulo equilatero es: %.2f", perimetro);
		
	}else{
		printf("%s", mensaje_error);
	}

	return 0;
}
