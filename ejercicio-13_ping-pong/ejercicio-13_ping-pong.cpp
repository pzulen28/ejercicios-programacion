#include <stdio.h>

int main(){
	int numero=0;
	char mensaje_error[]="El numero debe ser mayor que 1 y menor que 100";
	char mensaje_aviso[]="El numero ingresado no es divisible por 3 y 5 ";
	
	printf("Ingrese un numero: ");
	scanf("%d", &numero);
	if(numero>=1 && numero<=100){
		if(numero%3==0 && numero%5==0){
			printf("ping-pong");
		}else{
			if(numero%3==0){
				printf("ping");
			}else{
				if(numero%5==0){
					printf("Pong");
				}else{
					printf("%s", mensaje_aviso);
				}
			}
		}
	}else{
		printf("%s", mensaje_error);
		
	}
	

	
	return 0;
}
